export default function Footer() {
    return(
        <h6 className="footer mt-5 p-5 text-center text-secondary ">Copyright © 2021 All Rights Reserved.</h6>
    )
}