import { Form,Button,Row,Col  } from 'react-bootstrap';

export default function LoginForm() {
    return(
        <Col lg={5} className="p-5 m-auto shadow-lg rounded">
            <h5 className="text-center">Sign In to your account</h5>
            <Form  className="p-3 d-grid gap-2">
                <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" />
                    <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                    </Form.Text>
                </Form.Group>

                <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Remember Me" />
                </Form.Group>
                <Button  size="sm" variant="primary" type="submit">
                    Sign In
                </Button>
            </Form>
        </Col>
    );
}