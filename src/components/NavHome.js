import { Container,Navbar, Nav,NavDropdown,Button,Form,FormControl  } from 'react-bootstrap';

export default function NavHome(){
    return(
        <Navbar bg="primary" variant="dark" expand="lg" sticky="top">
            <Container fluid>
                <Navbar.Brand href="#">Ecommerce</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                <Nav
                    className="me-auto my-2 my-lg-0"
                    style={{ maxHeight: '100px' }}
                    navbarScroll
                >
                    <Nav.Link href="#action1">Product</Nav.Link>
                    <NavDropdown title="Category" id="navbarScrollingDropdown">
                    <NavDropdown.Item href="#action3">Categeory1</NavDropdown.Item>
                    <NavDropdown.Item href="#action4">Categeory2</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item href="#action5">
                        Something else here
                    </NavDropdown.Item>
                    </NavDropdown>
                </Nav>
                <Form className="d-flex">
                    <FormControl
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                    />
                    <Button variant="secondary">Search</Button>
                </Form>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}