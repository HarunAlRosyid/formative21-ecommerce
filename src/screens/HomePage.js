import { Container} from 'react-bootstrap';
import NavHome from '../components/NavHome';
import Footer from '../components/Footer';

export default function HomePage() {
    return(
        <>
            <NavHome/> 
            <Container fluid="md">
                <h3 className="text-center p-3">Welcome to the website</h3>
            </Container>
            <Footer/>
        </>
    )
}