import { Container, Row } from 'react-bootstrap';
import LoginForm from '../components/LoginForm';
import Footer from '../components/Footer';

export default function LoginPage() {
    return(
        <Container fluid="md">
            <Row className="mt-5">
                <LoginForm/>
            </Row>
           <Footer/>
        </Container>
    );
}
